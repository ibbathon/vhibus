import sys
import datetime
from datetime import date


# decorators
def add_this(val):
    def _decorator(method):
        def _wrapper(*args, **kwargs):
            eval_val = val
            if callable(val):
                eval_val = val()
            return f'{eval_val} {method(*args, **kwargs)}'
        return _wrapper
    return _decorator


NAMED = 'initial'


def return_unnamed():
    return 'initial'


def return_named():
    return NAMED


class DecoratorFiddling:
    @add_this(NAMED)
    def with_constant(self, intext):
        return intext

    @add_this(return_unnamed)
    def with_callable(self, intext):
        return intext

    @add_this(return_named)
    def with_callable_and_constant(self, intext):
        return intext


# what is happening
class WhatIsHappening:
    def some_func(self):
        return sys.modules[__name__]


# mock_open
class OpenAFile:
    def read_func(self, filename):
        with open(filename, 'r') as f:
            return f.read()

    def write_func(self, filename):
        with open(filename, 'w') as f:
            f.write('words words words')
