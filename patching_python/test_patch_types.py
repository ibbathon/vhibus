import os
import pytest
from patch_types import NormalExample, DictExample, ObjExample

my_home = os.getenv('HOME')


class TestPatchTypes:
    def test_mocks(self, mocker):
        mock_object = mocker.MagicMock()
        mock_object.sample_func(1, 'a', word='word')
        mock_object.sample_func.assert_called_once()
        assert mock_object.sample_func.call_args_list == [
            ((1, 'a'), {'word': 'word'}),
        ]

    def test_normal_patching(self, mocker):
        mock_date = mocker.patch(
            'patch_types.date',
            **{'today.return_value': '1970-01-01'}
        )
        rv = NormalExample().some_func()
        mock_date.today.assert_called_once_with()
        assert rv == '1970-01-01'

    def test_this_will_not_work(self, mocker):
        mock_date = mocker.patch(
            'datetime.date',
            **{'today.return_value': '1970-01-01'}
        )
        rv = NormalExample().some_func()
        mock_date.today.assert_called_once_with()
        assert rv == '1970-01-01'

    def test_dict_patching(self, mocker):
        new_env = {'UNKNOWN_ENVVAR': 'newval'}
        mocker.patch.dict('patch_types.os.environ', new_env)
        rv = DictExample().some_func()
        assert rv == (my_home, 'newval')

    def test_dict_patching_direct(self, mocker):
        new_env = {'UNKNOWN_ENVVAR': 'newval'}
        mocker.patch.dict('os.environ', new_env)
        rv = DictExample().some_func()
        assert rv == (my_home, 'newval')

    def test_dict_patching_clear(self, mocker):
        new_env = {'UNKNOWN_ENVVAR': 'newval'}
        mocker.patch.dict('patch_types.os.environ', new_env, clear=True)
        rv = DictExample().some_func()
        assert rv == ('oops', 'newval')

    def test_obj_patching(self, mocker):
        obj = ObjExample()
        with pytest.raises(Exception):
            obj.some_func()
        mocker.patch.object(obj, 'some_func', mocker.Mock(return_value=47))
        assert obj.some_func() == 47
