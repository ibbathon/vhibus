from tricky_stuff import DecoratorFiddling, WhatIsHappening, OpenAFile


class TestDecoratorFiddling:
    def test_with_constant_cannot_override(self, mocker):
        mocker.patch('tricky_stuff.NAMED', 'patched')
        rv = DecoratorFiddling().with_constant('param')
        # The naive assumption is that we should have 'patched param',
        # but the constant is passed to the decorator at module-initialization
        # time, **NOT** when we initialize the class/call the method.
        assert rv == 'initial param'

    def test_with_callable_only_cannot_override(self, mocker):
        mocker.patch('tricky_stuff.return_unnamed', lambda: 'patched')
        rv = DecoratorFiddling().with_callable('param')
        # Now the problem is that the decorator has a reference to the
        # original `return_unnamed` method, so when we override the one in the
        # module, we're not overwriting the one the decorator is using
        assert rv == 'initial param'

    def test_with_callable_and_constant_can_override(self, mocker):
        mocker.patch('tricky_stuff.NAMED', 'patched')
        rv = DecoratorFiddling().with_callable_and_constant('param')
        # The value method is not called until the main method is called, so
        # it grabs module.NAMED **after** we have changed it. So we finally get
        # the desired result.
        assert rv == 'patched param'


class TestWhatIsHappening:
    def test_patching_at_use(self, mocker):
        module_data = WhatIsHappening().some_func()
        print(f"Before patching: time: {module_data.datetime.time}; date: {module_data.date}")
        assert type(module_data.datetime.time) == type(TestDecoratorFiddling)  # i.e. 'type'
        assert type(module_data.date) == type(TestDecoratorFiddling)  # i.e. 'type'

        mocker.patch('tricky_stuff.datetime.time')
        mocker.patch('tricky_stuff.date')

        module_data = WhatIsHappening().some_func()
        print(f"After patching: time: {module_data.datetime.time}; date: {module_data.date}")
        assert str(type(module_data.datetime.time)) == str(type(mocker.MagicMock()))  # i.e. 'MagicMock'
        assert str(type(module_data.date)) == str(type(mocker.MagicMock()))  # i.e. 'MagicMock'

    def test_patching_at_definition(self, mocker):
        mocker.patch('datetime.time')
        mocker.patch('datetime.date')

        module_data = WhatIsHappening().some_func()
        print(f"After patching: time: {module_data.datetime.time}; date: {module_data.date}")
        assert str(type(module_data.datetime.time)) == str(type(mocker.MagicMock()))  # i.e. 'MagicMock'
        assert str(type(module_data.date)) == str(type(TestDecoratorFiddling))  # i.e. 'type'


class TestOpenAFile:
    def test_read_from_mock(self, mocker):
        mocked_context = mocker.mock_open(read_data='some stuff')
        mocker.patch('tricky_stuff.open', mocked_context)
        rv = OpenAFile().read_func('afilename')
        mocked_context.assert_called_once_with('afilename', 'r')
        assert rv == 'some stuff'

    def test_write_to_mock(self, mocker):
        mocked_context = mocker.mock_open()
        mocker.patch('tricky_stuff.open', mocked_context)
        OpenAFile().write_func('afilename')
        mocked_context.assert_called_once_with('afilename', 'w')
        mocked_context().write.assert_called_once_with('words words words')
