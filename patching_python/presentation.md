# VAULT
### Patching in Python

##### What will be covered
* Types of mocking/patching (MagicMock, patch, patch.dict, patch.object)
  * Where/how to mock/patch (package import vs class/object import)
* `return_value` vs. `side_effect` and when to use each
* How/when to use `assert_called...` and `call_args_list`
* In-depth studies:
  * Tricky situations with decorators and other class-level module initializations
  * Why you must mock/patch in particular ways (i.e. how it works)
  * What is a mock? What is patching? (some more details on the above)
  * When/how often are mocks created after patching?
  * `mock_open`

##### What won't be covered
* Non-decorator, non-mocker patching (too complex and unwieldy for this bus)
* Time-freezing (useful, but not immediately related)
* Mocking philosophies (integration vs unit tests)

##### Recording
[Slack link](https://vaulthealth.slack.com/archives/C012P711UF4/p1643648688711459?thread_ts=1643644163.315599&cid=C012P711UF4)


# Types of mocking/patching
* MagicMock/Mock
* patch
* patch.dict
* patch.object
* patch.multiple (not covered; only useful when doing context managers)

## MagicMock
* MagicMock is an object which overrides `__getattr__`
  * you can "access" any attribute/method on it without an error
  * it records method calls for later reference
* MagicMocks (and their lesser parent Mocks) are rarely used directly in our codebase
  * all normal patches produce a MagicMock, so knowing how to use them will help you
  * useful if you want a quick object with some attributes (see examples)

### Monorepo Examples
* [MagicMock as quick object with attributes](https://github.com/vaulthealth/monorepo/blob/main/server/tests/models/test_order.py#L60)
* [MagicMock to avoid DB calls](https://github.com/vaulthealth/monorepo/blob/main/server/tests/workflow/test_workflow_answer_key_validation.py#L11)
* [Fancier use of MagicMock to skip AWS](https://github.com/vaulthealth/monorepo/blob/main/server/tests/admin/test_pipelines.py#L163)
* [Using mocker instead of importing from unittest.mock](https://github.com/vaulthealth/monorepo/blob/main/server/tests/insurance/eligibility/test_waystar.py#L72)

## Normal patch
* Used extensively in our codebase
* Rule-of-thumb is "mock where it is used, not where it is defined"
  * [Python docs](https://docs.python.org/3/library/unittest.mock.html#where-to-patch)
  * See `test_this_will_not_work` in `test_patch_types.py`

### Monorepo Examples
* [Overriding subfunc call](https://github.com/vaulthealth/monorepo/blob/main/server/tests/utils/test_timezone_helpers.py#L18)
  * This is also a perfect example of when fixtures are called
* [Disable 3rd-party calls and capture logs](https://github.com/vaulthealth/monorepo/blob/main/server/tests/scheduling/test_telehealth_reminder.py#L75-L76)
* [Using MODULE constant to avoid typing out the module string repeatedly](https://github.com/vaulthealth/monorepo/blob/main/server/tests/admin/medical_records/test_routes.py#L30-L33)

## Dictionary patch
* Primarily used for `os.environ` and config patching
* The default of `clear=False` should almost always be used to avoid unexpected issues
* See `test_dict_patching_direct` for an example of when you can patch where it is defined, rather than where it is used

### Monorepo Examples
* [Overriding config](https://github.com/vaulthealth/monorepo/blob/main/server/tests/mental_health/test_mental_health.py#L300-L305)
* [Non-config example with clearing](https://github.com/vaulthealth/monorepo/blob/main/server/tests/test_api_appointments.py#L532)

## Object patch
* Primarily used in our codebase for overriding 3rd-party extension clients
* Used whenever you've imported the object you'll be testing (as opposed to calling something which calls the thing you need to mock)

### Monorepo Examples
* [Mocking out oauth](https://github.com/vaulthealth/monorepo/blob/main/server/integration_tests/test_oauth.py#L132)
* [Mocking a single method to focus testing](https://github.com/vaulthealth/monorepo/blob/main/server/tests/external_clients/workpath/acuity_synchronizer/test_scheduled_appointment_synchronizer.py#L78)
* [Patching a static method on a service we're directly calling](https://github.com/vaulthealth/monorepo/blob/main/server/tests/workflow/occurence_event/test_vaccination_status_answers_event_service.py#L92-L96)


# `return_value` vs `side_effect` vs `wraps`
* `return_value` should be your default choice
* `side_effect` is mainly used for three reasons:
  * raising an exception
  * returning a series of results on multiple calls
  * calling a method as return value (not used in our codebase?)
* `wraps` is for when you want call counts, but still want to actually call the method
  * Our codebase incorrectly uses `side_effect` for this
  * There doesn't appear to be a functional difference, but `wraps` makes it clearer what we're doing

### Monorepo Examples
* See previous examples for how to use `return_value`
* [side_effect raising an exception](https://github.com/vaulthealth/monorepo/blob/main/server/tests/insurance/eligibility/test_waystar.py#L64)
* [side_effect returning a series of results](https://github.com/vaulthealth/monorepo/blob/main/server/tests/insurance/eligibility/test_waystar.py#L74)
* [A more complex series of results](https://github.com/vaulthealth/monorepo/blob/main/server/tests/test_s3.py#L562-L565)
* [side_effect wrapping a method (should use wraps instead)](https://github.com/vaulthealth/monorepo/blob/main/server/integration_tests/test_spectrum.py#L140)


# `assert_called...` and other verifications
* The various assertion methods on Mock objects
* `call_count`, `call_args`, and `call_args_list`

## Assertion methods (which to use)
* `assert_called_once_with` checks if the method was called exactly once with given params
  * Your main bread-and-butter assertion method. This and `call_args_list` will be your assertion methods for 99.9% of your tests.
  * Lots of examples throughout monorepo, but the Python docs also have some [good examples](https://docs.python.org/3/library/unittest.mock.html#unittest.mock.Mock.assert_called_once_with)
* `assert_not_called` checks if a mocked method was never called
  * Effectively the only other `assert_...` method you should use

* `assert_called` checks if the method was ever called
* `assert_called_once` checks if the method was called exactly once
* `assert_called_with` checks if the last call to the method had the given params
* `assert_any_call` is similar to `assert_called_with`, but checks if *any* call had those params
* `assert_has_calls` checks if a list of calls happened (optionally in-order)
  * All of these are terrible for the same reason: they let unexpected behavior through without notifying you

## Call counts and args
* `call_count` records how many times a method was called
  * e.g. `assert mock_func.call_count == 3`
  * Useful for when you've already tested param input elsewhere, but need to test multiple calls and don't want to write out the params for every call
  * Also useful as a preliminary assertion to stop execution before a larger assertion would flood the console with output
* `call_args` is the list of params passed to the method on the *last* call to the method
  * I recommend avoiding this and just using `call_args_list[-1]` for ease of memory
* `call_args_list` is the list of lists of params for each call to a method
  * Useful when your method is called multiple times and you need to assert the params for every call (use `assert_called_once_with` if you only have one call)

### Monorepo Examples
* [Asserting multiple logger calls](https://github.com/vaulthealth/monorepo/blob/main/server/tests/admin/medical_records/test_bulk_export_processor.py#L149-L156)
* [Verifying a specific arg in a larger call](https://github.com/vaulthealth/monorepo/blob/main/server/tests/tasks_process_message/test_process_data_sync_event.py#L50-L53)
* [Grabbing the results of a mock-call to simulate untested behavior](https://github.com/vaulthealth/monorepo/blob/main/server/tests/test_api_covid.py#L482)
* [Checking call_count before a more complex verification](https://github.com/vaulthealth/monorepo/blob/main/server/tests/medical_records/test_medical_records_processor.py#L207)
* [Removing/modifying troublesome args to allow testing the others](https://github.com/vaulthealth/monorepo/blob/main/server/tests/medical_records/test_medical_records_processor.py#L208-L218)


# In-depth Studies
* Patching decorator inputs
* Why we mock at use instead of at definition
  * What is patching actually doing?
* Comparing mocks and sub-mocks (in particular, calls to mock attributes vs calls to mock generators)
  * Why you shouldn't trust `==` between non-constants in tests
* `mock_open` and other context managers
  * [Python docs on mock_open](https://docs.python.org/3/library/unittest.mock.html#mock-open)


# THANK YOU!
### Q&A
